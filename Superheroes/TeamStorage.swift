//
//  TeamStorage.swift
//  Superheroes
//
//  Created by Abu_zakariya on 23.01.2022.
//

import Foundation

class TeamStorage {
    
    static let shared = TeamStorage()

    var teams: [Team] = []
    
    private init() {}
    
}
