//
//  SceneDelegate.swift
//  Superheroes
//
//  Created by Abu_zakariya on 20.01.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    private var coordinator: MainCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let commandListNavigationController = UINavigationController()
        commandListNavigationController.navigationBar.tintColor = .black
        
        coordinator = MainCoordinator(navigationController: commandListNavigationController)
        coordinator?.start()

        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = commandListNavigationController
        window?.makeKeyAndVisible()
    }


}

