//
//  CommandListTableViewCell.swift
//  Superheroes
//
//  Created by Abu_zakariya on 20.01.2022.
//

import UIKit

final class CommandListTableViewCell: UITableViewCell {
    
    static let reuseId = "CommandListTableViewCell"
    
    private let teamNameLabelLeadingOffset: CGFloat = 16
    private let teamNameLabelTopOffset: CGFloat = 16
    
    private let membersCountLabellTopOffset: CGFloat = 4
    
    private let teamNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let membersCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .systemGray
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupContent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupContent() {
        contentView.addSubview(teamNameLabel)
        contentView.addSubview(membersCountLabel)
        
        teamNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: teamNameLabelLeadingOffset).isActive = true
        teamNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: teamNameLabelTopOffset).isActive = true
        
        membersCountLabel.leadingAnchor.constraint(equalTo:  teamNameLabel.leadingAnchor).isActive = true
        membersCountLabel.topAnchor.constraint(equalTo: teamNameLabel.bottomAnchor, constant: membersCountLabellTopOffset).isActive = true
    }
    
    func configure(item: Team) {
        teamNameLabel.text = item.name
        membersCountLabel.text = "Количество героев: \(item.heroes.count)"
    }
    
}
