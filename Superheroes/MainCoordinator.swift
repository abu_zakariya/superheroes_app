//
//  MainCoordinator.swift
//  Superheroes
//
//  Created by Abu_zakariya on 26.01.2022.
//

import UIKit

final class MainCoordinator {
    
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let controller = CommandListViewController()
        controller.coordinator = self
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showTeamInfo(team: Team) {
        let controller = TeamInfoViewController(team: team)
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showTeamModify(team: Team) {
        let controller = ChangeTeamViewController(team: team, type: .modify)
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showTeamCreate() {
        let controller = ChangeTeamViewController(team: nil, type: .new)
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showAddHeroViewController(delegate: ChangeTeamViewController) {
        let controller = AddHeroViewController()
        controller.delegate = delegate
        controller.coordinator = self
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    func popToRootViewController() {
        guard let rootViewContoller = navigationController.viewControllers.first as? CommandListViewController else {
            return
        }

        rootViewContoller.reloadData()
        navigationController.popToRootViewController(animated: true)
    }
    
    func popViewController() {
        navigationController.popViewController(animated: true)
    }
    
}
