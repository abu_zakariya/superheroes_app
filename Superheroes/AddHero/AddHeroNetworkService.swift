//
//  AddHeroNetworkService.swift
//  Superheroes
//
//  Created by Abu_zakariya on 24.01.2022.
//

import UIKit

final class AddHeroNetworkService {
    
    weak var delegate: AddHeroViewController?
    
    private let decoder = JSONDecoder()
    
    func sendGenerateRequest() {
        guard let url = URL(string: "https://random-word-api.herokuapp.com/word?number=5") else { return }
        
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            if error != nil {
                return print("Request failed")
            }
            
            guard let data = data else { return }
            
            guard let strings = try? self?.decoder.decode([String].self, from: data) else { return }
            
            DispatchQueue.main.async {
                self?.delegate?.updateNewHeroContent(strings: strings)
            }
        }.resume()
        
    }
    
}
