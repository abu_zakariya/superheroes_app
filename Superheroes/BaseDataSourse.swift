//
//  BaseDataSourse.swift
//  Superheroes
//
//  Created by Abu_zakariya on 22.01.2022.
//

import UIKit

class BaseDataSource {
    
    var itemsCount = 1
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        fatalError("Метод должен быть переопределен")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        nil
    }
    
    func valueChanged(value: String) {
        fatalError("Метод должен быть переопределен")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        fatalError("Метод должен быть переопределен")
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        fatalError("Метод должен быть переопределен")
    }
    
    func makeLeader(cell: UITableViewCell) {
        fatalError("Метод должен быть переопределен")
    }
    
}
