//
//  Models.swift
//  Superheroes
//
//  Created by Abu_zakariya on 24.01.2022.
//

final class Team {
    
    var name: String
    var heroes: [Hero]
    
    init(name: String, heroes: [Hero]) {
        self.name = name
        self.heroes = heroes
    }
    
}

final class Hero {
    
    var name: String
    var phrase: String
    var equipment: String
    
    init(name: String, phrase: String, equipment: String) {
        self.name = name
        self.phrase = phrase
        self.equipment = equipment
    }
    
}
