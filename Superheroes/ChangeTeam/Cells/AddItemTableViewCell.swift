//
//  AddWordTableViewCell.swift
//  BicMaBe
//
//  Created by Abu_zakariya on 18.01.2022.
//  Copyright © 2022 Эли Албертов. All rights reserved.
//

import UIKit

final class AddItemTableViewCell: UITableViewCell {
    
    static let reuseId = "AddWordTableViewCell"
    
    private let plusImageLeadingOffset: CGFloat = 72
    private let plusImageTopOffset: CGFloat = 4
    
    private let addWordLabelLeadingOffset: CGFloat = 8

    private let plusImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "plus")
        return image
    }()

    private let addWordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        
        contentView.addSubview(plusImageView)
        contentView.addSubview(addWordLabel)
        
        plusImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: plusImageLeadingOffset).isActive = true
        plusImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: plusImageTopOffset).isActive = true
        
        addWordLabel.leadingAnchor.constraint(equalTo: plusImageView.trailingAnchor, constant: addWordLabelLeadingOffset).isActive = true
        addWordLabel.topAnchor.constraint(equalTo: plusImageView.topAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(item: String) {
        addWordLabel.text = item
    }
}
