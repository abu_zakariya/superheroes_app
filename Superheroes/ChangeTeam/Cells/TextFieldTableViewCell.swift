//
//  AddWordTableViewCell.swift
//  BicMaBe
//
//  Created by Abu_zakariya on 18.01.2022.
//  Copyright © 2022 Эли Албертов. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    static let reuseId = "TextFieldTableViewCell"
    
    weak var delegate: BaseDataSource?
    
    private let textFieldLeadingOffSet: CGFloat = 16
    private let textFieldTrailingOffset: CGFloat = -72
    private let textFieldVerticalOffset: CGFloat = 4
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.layer.cornerRadius = 8
        textField.clipsToBounds = true
        return textField
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(textField)
        backgroundColor = .clear
        
        textField.delegate = self
        
        textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: textFieldLeadingOffSet).isActive = true
        textField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: textFieldVerticalOffset).isActive = true
        textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: textFieldTrailingOffset).isActive = true
        textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -textFieldVerticalOffset).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(item: String?) {
        textField.text = item
    }
    
}

extension TextFieldTableViewCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.valueChanged(value: textField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
